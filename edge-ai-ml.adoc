= Edge AI/ML Solutions
Ishu Verma  @ishuverma, William Henry @ipbabble,
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify

This page contains solutions that follow the pattern of a single datacenter and multiple smaller edge locations that run small application clusters.

Currently there are two use cases:


* Manufacturing Factory Anomaly Detection
* Medical Facility Medical Diagnosis

== Edge AI/ML Industrial Edge


The manufacturing industry has consistently used technology to fuel innovation, production optimization and operations. Now, with the combination of edge computing and AI/ML, manufacturers can benefit from bringing processing power closer to data that, when put into action faster, can help to proactively discover of potential errors at the assembly line, help improve product quality, and even reduce potential downtime through predictive maintenance and in the unlikely event of a drop in communications with a central/core site, individual manufacturing plants can continue to operate. Red Hat's has successfully implemented AI/ML at the edge in manufacturing using OpenShift, Application Services  including AMQ, Kafka and Camel K, Quay Container Registry and OpenShift Container Storage.

Industrial Edge 2.0 is a Red Hat validated pattern. More details can be found http://hybrid-cloud-patterns.io/industrial-edge[here]

==== Diagrams

https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/industrial-edge-v2.drawio[Load Diagram]


https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/industrial-edge-v2.drawio?inline=false[Download Diagram]

*Logical Diagram* +

image:logical-diagrams/industrial-edge-ld.png[1500,1000]

'''

*Schematic Diagram* +

image:schematic-diagrams/industrial-edge-devops.png[1500, 1000]

'''
image:schematic-diagrams/industrial-edge-gitops.png[1500, 1000]


*Detail Diagrams* +

image:detail-diagrams/Mfg-AI-ML/AMQ-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Anomaly-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Anomaly-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/CI_CD-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Dashbrd-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Dashbrd-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Dist-Strm-CDC.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Dist-Strm-Detl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Edge AI ML flow.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/GitOps-agent-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/GitOps-contrlr-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/ACM.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Gitrepo-CDC-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Gitrepo-Fact-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/ImageRegistry-CDC-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/ImageRegistry-Cloud-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/ImageRegistry-Fact-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Line-server-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Mqtt-intg-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Msg-consm-Dtl.png[450, 300]
image:detail-diagrams/Mfg-AI-ML/Strm-proc-Dtl.png[450, 300]
--
