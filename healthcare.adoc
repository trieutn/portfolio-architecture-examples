= Healthcare
Eric D. Schabell @eschabell
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


Our world transformed overnight, and healthcare had to respond immediately. Now more than ever, it's critical to have access to data
and unlimited scale, and to design and modify decisioning, processes, and workflows for specific situations—without being
vendor-dependent. https://www.redhat.com/en/solutions/healthcare[Red Hat] is here to help with a flexible, open approach.

This document is a collection of various architectures in the healthcare domain. Each case is detailed below with a definition of the 
use case along with logical, schematic, and detailed diagrams.

Open the diagrams below directly in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your content.


== Intelligent data as a service (iDaaS)

Use case: Intelligent DaaS (Data as a Service) is about building and delivery of systems and platforms in a secure and scalable
manner while driving data needs for moving towards consumerisation in healthcare.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/idaas.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/idaas.drawio?inline=false[[Download Diagram]]
--

--
image:intro-marketectures/idaas-marketing-slide.png[750,700]
--

--
image:logical-diagrams/idaas-ld.png[350, 300]
--


== Edge medical diagnosis

Use case: Accelerating medical diagnosis using condition detection in medical imagery with AI/ML at medical facilities.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/edge-medical-diagnosis.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/edge-medical-diagnosis.drawio?inline=false[[Download Diagram]]
--

--
image:intro-marketectures/edge-medical-diagnosis-marketing-slide.png[750,700]
--

--
image:logical-diagrams/edge-medical-diagnosis-ld.png[350, 300]
image:logical-diagrams/edge-medical-diagnosis-details-ld.png[350, 300]
--

--
image:schematic-diagrams/edge-medical-diagnosis-network-sd.png[350, 300]
image:schematic-diagrams/edge-medical-diagnosis-data-sd.png[350, 300]
image:schematic-diagrams/edge-medical-diagnosis-gitops-sd.png[350, 300]
image:schematic-diagrams/edge-medical-diagnosis-gitops-data-sd.png[350, 300]

--

--
image:detail-diagrams/edge-medical-diagnosis-xray-server.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-notification.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-ml-cicd.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-detection.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-streaming-datacenter.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-streaming-facility.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-registry-cloud.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-registry-datacenter.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-registry-edge.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-scm-datacenter.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-scm.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-gitops-controller.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-gitops.png[250, 200]
image:detail-diagrams/edge-medical-diagnosis-database.png[250, 200]
--


