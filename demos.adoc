= Product demo diagrams
 Eric D. Schabell @eschabell
:homepage: https://gitlab.com/redhatdemocentral/portfolio-architecture-examples
:imagesdir: images
:icons: font
:source-highlighter: prettify


The following are a collection of diagrams used in product demos and documentation.

== Product demo architecture diagrams

Open the diagrams in the diagram tooling using 'Load Diagram' link. To download the project file for these diagrams use
the 'Download Diagram' link. The images below can be used to browse the available diagrams and can be embedded into your
content.


=== CodeReady Containers with Red Hat Signal Marketing Demo

Use case: Process automation using a signal to trigger separate process actions.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhpam-signal-marketing-demo.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/crc-rhpam-signal-marketing-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/crc-rhpam-signal-marketing-demo.png[350, 300]
--


=== CodeReady Containers with Red Hat Quick Loan Bank Demo

Use case: Process automation with front end integration, human tasks, rules, and DMN.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhdm-quick-loan-bank-demo.drawio[[Load Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/crc-rhdm-quick-loan-bank-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/crc-quick-loan-bank-demo.png[350, 300]
--


=== CodeReady Containers with Mortgage Process Demo

Use case: Process automation featuring human tasks, rules, and forms design including workshop.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhpam-mortgage-demo.drawio[[Load
Diagram]]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/crc-rhpam-mortgage-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/crc-rhpam-mortgage-demo.png[350, 300]
--


=== CodeReady Containers with Red Hat Decision Manager Install

Use case: Getting started installing Decision Manager product.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhdm-install-demo.drawio[[Load Diagram]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/crc-rhdm-install-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/crc-rhdm-install.png[350, 300]
--


== CodeReady Containers with Red Hat Process Automation Manager Install

Use case: Getting started installing process automation product.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/crc-rhpam-install-demo.drawio[[Load Diagram]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/crc-rhpam-install-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/crc-rhpam-install.png[350, 300]
--

== Agile Integration Event Streaming Hazard Demo 

Use case: Agile integration using integration products.


--
https://redhatdemocentral.gitlab.io/portfolio-architecture-tooling/index.html?#/portfolio-architecture-examples/projects/event-streaming-hazard-demo.drawio[[Load Diagram]
https://gitlab.com/redhatdemocentral/portfolio-architecture-examples/-/raw/main/diagrams/product-demos/event-streaming-hazard-demo.drawio?inline=false[[Download Diagram]]
--

--
image:product-demo-diagrams/event-streaming-hazard-demo.png[350, 300]
--

